import random

from pacman_module.game import Agent
from pacman_module.pacman import Directions
from ActionList import listD

class Node():
"""
One node must contain a state, the link to the super-parent, links to
all children, links to grand-children
"""

    def __init__(self,state,parent=None,iswin=FALSE,islost=FALSE):
        self.state=state
        self.parent=parent
        self.grandparent=parent.grandparent
        if self.grandparent is None:
           self.grandparent=self.parent
        self.children=list();
        self.gandchild=list()
        self.iswin=iswin
        self.islost=islost

    def removeYourself(self,state):
    """
    removes the node from the tree and returns the merged nodes
    that correspond to the current state if there is any
    """
        l=list()
        for child in self.children:
            if child.state==state:
                l.add(child);

        if len(l)==0:
            return Node(state)
        elif len(l)!=1:
            for(i=1;i<len(l);i=i+1):
                l[0].addNeighbour(l[i])


        return l[0]


    def getLastChildren(self):
    """
    returns the list of final states that exist for children
    """
        return self.grandchild


    def getDirections(self,agent=0):
    """
    returns the list of numbers of directions already expanded,
    order : North, East, South, West
    """
        l=[0,0,0,0]
        for c in self.children:
            v=c.state.getPosition(agent)
            v=v-self.state.getPosition(agent)
            # not sure this works, but it is free to try
            if v[0]>0:
                l[1]+=1
            elif v[0]<0:
                l[3]+=1
            elif v[1]>0:
                l[0]+=1
            else:
                l[2]+=1
            return l

    def expandRandom(self):
    """
    expands the node in a random direction
    returns the child created
    This DOES NOT take care of grandchildren
    """
        a =-1
        while (a==-1) or (self.getDirections()[a]==1) :
            a=random.randint(0,num-1)
        return self.expandOnce(a)


    def expandOnce(self,direct):
    """
    expands the thing once in a given direction
    returns the child created
    This DOES NOT take care of grandchildren!
    """
        num_agents=len(self.state.agentStates)
        # generateSuccessor for pacman
        state=self.state.generatePacmanSuccessor(listD.translate(direct))
        islost=FALSE
        iswin=FALSE
        if state.isWin():
            iswin=TRUE
        if state.isLose():
            islost=TRUE

        for i in range(1,num_agents):
            num=len(self.state.getLegalActions(i))
            a=-1
            while (a==-1) or (self.getDirections(i)[a]==1) :
                a=random.randint(0,num-1)
                state=state.generateSuccessor(i,self.translate(i,a))
                if state.isWin():
                    iswin=TRUE
                if state.isLose():
                    islost=TRUE
        length=len(self.children)
        self.children.append(Node(state,self,iswin,islost))
        return self.children[length]


    def expand(self,direct,depth):
    """
    expands the node once in the said direction then depth-1 times
    Except if the pacman loses, obviously
    """
        # I make a loop so as to avoid depths limits
        child=self.expandOnce(direct);
        child2=child;
        for i in range(1,depth):
            if !child.iswin and !child.islost:
                child=child.expandRandom();
        child2.correctDescendance();


    def getOptimalDirection(self):
    """
    returns the direction with maximum average score
    """
        # 1) sort children by the direction they took
        N=list()
        E=list()
        S=list()
        W=list()
        L=[N,E,S,W] # not sure this works, free to try
        L2=copy.deepCopy(L);
        for c in self.children:
            v=c.state.getPosition()
            v=v-self.state.getPosition()
            # not sure this works, but it is free to try
            if v[0]>0:
                E.append(c)
            elif v[0]<0:
                W.append(c)
            elif v[1]>0:
                N.append(c)
            else:
                S.append(c)
        # 2) list grandchildren by direction
        # 3) count losses and wins
        for i in range(0,4):
            L2[i]=[0,0,0] #win, loss, ?
            for c in L[i]:
                gc=c.getLastChildre        score=[0,0,0,0]n()
                for g in gc:
                    if g.iswin:
                        L2[i][0]+=1
                    elif g.islost:
                        L2[i][1]+=1
                    else:
                        L2[i][2]+=1
        # 4) intelligently design decision
        max=0;
        maxscore=0;
        for j in range(0,4)
            score=L2[j][0]*10+L2[j][1]*1000+L2[j][2]
            if score>=maxscore:
                maxscore=score
                max=j
        return listD.translate(j);

    def addNeighbour(self,neighbour):
    """
    fuse itself with the neighbour by adding the neighbours'children
    """
        self.children.extend(neighbour.children)

    def translate(self,agent,num):
    """
    Transforms a number n into the nth direction from getLegalActions
    for an agent. This is different from listD.translate(number).
    """
        return self.getLegalActions()[num]

    def correctDescendance(self):
    """
    This function determines the grandchild of self and adds it to its
    entire descendance
    """
        # again, I do it by looping in case there is a limit to
        # recursion
        current=self.children
        # check emptiness weirdly
        if not current:
            return list();

        definitive=list()

        while current2:
            current2=list()
            for child in current:
                if not child.children
                    definitive.append(child)
                else:
                    current2.extend(child.children)
            current=current2
