I haven't begun this thing yet, but here is an overview of what I intend to do :

## Code organisation

The code for the Minimax agent will be in pacmanPruning.py, the one for Monte-Carlo will be in pacmanMonteCarlo.py, the default launched by doing run.py is pacmanPruning, not the humanagent as initially coded. 

## Minimax

I intend to develop the minimax agent with alpha-beta pruning and a heuristic approxmation, which would be a mix between the 2 last documents asked in the part two of the project.

## Monte-Carlo

Since the Monte-Carlo method seems amusing to develop, I'll probably do that too.
