import random

from pacman_module.game import Agent
from pacman_module.pacman import Directions

from MonteCarloNode import Node
from ActionList import ListD

class MonteCarlo():

    def __init__(self,depth,state,width=20):
        """
            depth=number of turns to try
            width=number of tries for each possible direction
        """
        self.depth=depth
        self.width=width
        self.tree=Node(state)


    def setWidthFix(self,width):
        self.width=width

    def setWidthGhost(self, numGhosts, numPerGhost=20):
        """
            numGhost=number of ghosts
            numPerGhost= number of tries to give for each ghost
        """
        self.width=numGhost*numPerGhost

    def simul(self, state):
        """
            state is the current state
            returns the optimal result for the simulation
        """
        self.tree=self.tree.removeYourself()
        # I make a while rather than a recursion because otherwise the
        # user might ask for more recursion than Python can handle
        # (after all, Monte Carlo works best with bigger numbers)

        # I decide to make width*number of possible directions tries in
        # total, not for each expanded node, because my computer
        # probably can't handle O(width^depth) nodes...

        lasts=self.tree.getLastChildren()
        dir=self.tree.getDirections()
        for n in lasts:
            n.expandRandom()

        legal=state.getLegalActions()

        for i in range(4):
            if legal.count(ListD.directions[i])>0:
            # technically it would probably be faster to transform legal
            # into a list of booleans saying whether or not to go on,
            # but it's not the speed limiting factor anyways
                exp=self.width-dir[i]
                for j in range(exp):
                    self.tree.expand(ListD.directions[i], self.depth)

        return self.tree.getOptimalDirection()
