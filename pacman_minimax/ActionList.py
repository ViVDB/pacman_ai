from pacman_module.pacman import Directions

class ListD():
    directions={Directions.NORTH,
                Directions.EAST,
                Directions.SOUTH,
                Directions.WEST}

    def translate(number):
    """
    translate a number into a direction
    """
        return directions[number]
