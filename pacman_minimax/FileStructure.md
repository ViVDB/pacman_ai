
##### Action List

Gets directions in the right order so that coding the order is unified over all files.

##### run.py

That's the initial file to launch the game, modified to avoid repeating too many options.

##### Monte-Carlo

+ pacmanMonteCarlo.py

This is the file called by run.py, all it does is structuring calls to other files.

+ MonteCarlo.py

This is the file managing the nodes, by taking care of the transition between turns.

+ MonteCarloNode.py

This is where magic (should) happen, in this files nodes relate to each other to find the best outcome at each turn.

##### Pruning

+ pacmanPruning.py
