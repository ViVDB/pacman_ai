# Complete this class for all parts of the project

from pacman_module.game import Agent

from pacman_module.pacman import Directions

from node_ucs import Node

import sys

class PacmanAgent(Agent):
    def __init__(self, args):
        """
        Arguments:
        ----------
        - `args`: Namespace of arguments from command-line prompt.
        """
        self.args = args
        # path contains the directions in a list, last move is [0] and
        # first move is last.
        self.path=None
        sys.setrecursionlimit(3000)

    def get_action(self, state):
        """
        Given a pacman game state, returns a legal move.

        Arguments:
        ----------
        - `state`: the current game state. See FAQ and class
                   `pacman.GameState`.

        Return:
        -------
        - A legal move as defined in `game.Directions`.
        """
        if self.path is None:
            # make the tree and register the path
            # I want to make it only through recursion
            node=Node(state)
            self.path=node.expand()
        return self.path.pop()
