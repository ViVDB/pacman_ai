import heapq
import numpy as np

from pacman_module.game import Agent
from pacman_module.game import Actions

from pacman_module.pacman import GameState
from pacman_module.pacman import Directions

# a node is not a state because python doesn't support it well, but it
# works the same way
class Node():
    """
    I make a node only if it's a good idea to add it in the list !
    """
    grid=None
    first=None
    end=None
    list=[]
    def __init__(self, state, past_link=None):

        # this first because that way I do as if a Node is a state
        self.state=state
        # this because it will be changed for the first node
        self.going_to=self.getLegalPacmanActions()
        if not self.isWin():
            self.going_to.remove(Directions.STOP)
        else:
            Node.end=self
        self.coming_from=None
        # this then because it's different for the first node
        # or a node with food
        if Node.grid is None:
            # Node.grid= ([[None]* self.getWalls().height]
                               # * self.getWalls().width )
            Node.grid=np.empty((self.getNumFood()+1,self.getWalls().width,self.getWalls().height),dtype=Node,order='F')
            Node.first=self
            heapq.heapify(Node.list)
        else:
            self.coming_from= (Directions.REVERSE[
                    self.getPacmanState().getDirection()])
        # this because we need to allow coming back when there
        # is food
        if self.had_food() is False and self.coming_from is not None:
            self.going_to.remove(self.coming_from)
        # this because we don't care about anything here
        self.link_from=past_link
        # you don't need this because you don't need to go back to
        # forward nodes
        # if you need to delete yourself from the expansion,
        # you just remove yourself from the list
        # if you need to remove yourself from the grid...
        # you don't need to remove yourself from the grid, since
        # you always replace a higher cost node if you make
        # it to the grid.
        # self.link_next=[]

        # cost : to minimize
        self.cost=-self.getScore()
        posit=self.getPacmanPosition()
        try:
            if Node.grid[Node.first.getNumFood()-self.getNumFood()][posit[0]][posit[1]].cost>=self.cost:
                Node.grid[Node.first.getNumFood()-self.getNumFood()][posit[0]][posit[1]]=self
                heapq.heappush(Node.list,self)
        except AttributeError:
            Node.grid[Node.first.getNumFood()-self.getNumFood()][posit[0]][posit[1]]=self
            heapq.heappush(Node.list,self)

    def __lt__(self, other):
        return self.cost < other.cost

    def __str__(self):
        return self.state.__str__()

    ###########################
    # Here is the code :      #
    ###########################

    def expand(self):
        """Returns a list of nodes expanded through the A* algorithm
        Cost : score (to maximize, so -score to minimize)
        Heuristic : if there were no walls, the amount of
            squares that pacman needs to go through to get all
            the food, minus the number of food *100
        Last move first in the list of moves
        """
        path=[]
        if self.isWin() is not True:
            # (with a stopping condition : if self.isWin)
            # (you can do that because you're the lowest in the list
            # yourself !)
            #remove yourself because you can't expand twice :
            heapq.heappop(Node.list)
        # 1) next_nodes
        # this means you check for cycles in the next_nodes function
            self.next_nodes()
        # 2) ask the best in the list to expand
            # ask the best placed to expand but don't remove it since
            # it will remove himself

            path=Node.list[0].expand()

        '''
        # 3) get_path (partial path)
        temp=self.get_path()
        path.extend(temp)
        # 4) return path
        return path
        '''
        if self.link_from is None:
            return Node.end.get_path()
    def next_nodes(self):
        """
        this has the responsibility to find the next nodes and
        to remove them if need be from the list or the Grid
        """
        '''
        check for : -preceding node with lower or equal cost
        (lower or equal so that in the best case it's ok to go back on
        your feet)
                    -dead ends
                    -squares that aren't intersect but have food
        '''
        potential=[]
        for action in self.going_to:
            potential.append(self.generatePacmanSuccessor(action))
        # I have a list of nodes
        # for each one of them I need to go further
        for state in potential:
            # if there is : Directions.STOP
            #               where I come from
            #               where I can go to
            #               no food where I am
            listing=state.getLegalActions()
            while (len(listing)==3
                  and not self.hasFood(state.getPacmanPosition()[0],
                                     state.getPacmanPosition()[1])):
                listing.remove(Directions.STOP)
                listing.remove(Directions.REVERSE[
                                state.getPacmanState()
                                     .getDirection()])
                state= state.generatePacmanSuccessor(listing[0])
                listing=state.getLegalActions()
            # I now have a successor that's either:
            #   -on a food dot
            #   -in a dead end
            #   -at an intersect
            # if dead end -> do nothing
            # else -> make node here if node from grid permits
            if (len(listing)>3
                or self.hasFood(state.getPacmanPosition()[0],
                                state.getPacmanPosition()[1])):
            # I need to check for the grid in the init because
            # heuristic is a method from Node, not GameState
                Node(state,self)


    def get_path(self):
        """
        returns the partial path to self from link_from
        last move first
        """
        if self.link_from is None:
            return []
        path=[Directions.REVERSE[self.coming_from]]
        # I need the walls map to get the path
        walls=self.getWalls()
        posit=[self.getPacmanx(),self.getPacmany()]
        posit2=posit
        if path[0]==Directions.NORTH:
            posit2=[posit2[0],posit2[1]-1]
        elif path[0]==Directions.SOUTH:
            posit2=[posit2[0],posit2[1]+1]
        elif path[0]==Directions.EAST:
            posit2=[posit2[0]-1,posit2[1]]
        else:
            posit2=[posit2[0]+1,posit2[1]]
        final=self.link_from.getPacmanPosition()
        while (final[0]!=posit2[0] or final[1]!=posit2[1]):
            neighbours=Actions.getLegalNeighbors(posit2,walls)
            #remove the square you're in
            neighbours.remove((posit2[0],posit2[1]))
            if (neighbours[0][0]!=posit[0] or
                neighbours[0][1]!=posit[1]):
                posit=posit2
                posit2=neighbours[0]
            else:
                posit=posit2
                posit2=neighbours[1]
            vect=[posit[0]-posit2[0],posit[1]-posit2[1]]
            path.append(Actions.vectorToDirection(vect))
        path2=self.link_from.get_path()
        path.extend(path2)
        return path

    def had_food(self):
        """returns true if this node had food"""
        if Node.first is None:
            return False
        return Node.first.hasFood(self.getPacmanx(),
                                  self.getPacmany())

    #############################
    # Here is the OOP emulation #
    #############################

    def getWalls(self):
        return self.state.getWalls()

    def getPacmanx(self):
        return self.state.getPacmanPosition()[0]

    def getPacmany(self):
        return self.state.getPacmanPosition()[1]

    def getLegalPacmanActions(self):
        return self.state.getLegalPacmanActions()

    def getPacmanPosition(self):
        return self.state.getPacmanPosition()

    def hasFood(self,x,y):
        return self.state.hasFood(x,y)

    def getScore(self):
        return self.state.getScore()

    def isWin(self):
        return self.state.isWin()

    def getFood(self):
        return self.state.getFood()

    def getNumFood(self):
        return self.state.getNumFood()

    def isLose(self):
        return self.state.isLose()

    def generatePacmanSuccessor(self,action):
        return self.state.generatePacmanSuccessor(action)

    def generatePacmanSuccessors(self):
        return self.state.generatePacmanSuccessors()

    def getPacmanState(self):
        return self.state.getPacmanState()
