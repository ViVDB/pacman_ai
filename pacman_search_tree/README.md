This project uses python 3.6

# A* - pacmanagent.py

This is an implementation of the A* algorithm from the assignment.
I made it entirely through recursion, just for fun although loops would be more efficient, and also because I don't usually use recursion.

To prevent loops, the search tree is built directly on top of  grid having the same dimensions as the maze. Since you may need to go back on track when you get a dot, a new grid is needed when a dot diappears. To avoid making a huge number of grids, I just made one grid per number of dots possible on the grid (from 1 to the initial number).

This means that in some cases (as in the medium layout and the notstraight layout) the solution found isn't optimal. If I didn't do that, I would get 2^n possible grids (or a bit less if I kind of analyze clustering in advance, which is bad for bigger mazes (let's say a 100 dots), although it would probably be okay for the mazes tested. One possible solution to this problem would be to create the grids on the fly. Another solution may be to use one grid, but to have it as a grid of lists of lists of nodes, and hash the getFood() to compare the node to equivalent nodes in a reasonable time. Finally, a last proposition is to just remove all grids whatsoever, and directly hash a mix of getFood() and the position into a list of lists of visited nodes to be able to compare relatively fast.

In the loop_condition branch I tried to solve this, but it definitely only works for the small maze where I recreated the problem; a more complex maze wouldn't require to compare just to the lowest value of a heap but to all values, which would take too much time if there are lots of dots, although it would be better than making 2^100 grids, I guess. Anyways, I won't work on this anymore since it isn't essential to me, I'm just happy to have made a recursion once and discovered its limitations.

The choice of recursion and that choice to avoid cycles make the bigger mazes (medium and large layouts) run out of recursion, which stops them to work. Although the algorithm definitely works, since in run with the small2 layout, Python 3.6 (or my computer, for all I know) has a maximum number of recursion which I reached. But solving this would mean write it in loops, which I'm unwilling to do for this project.

In any case, the algorithm must not be that bad since it solves the medium maze in less than a second on my normally powered computer.

# A* no layer - pacman_no_layer.py

This is basically an implementation of A\*, but when in A\* you need to change the grid when you eat a dot, I didn't do this here. This means that, when there are lots of dots to eat, some states overwrite each other or don't write themselves and the list of states you can expand ends up too short. Of course, if you're lucky, it can still run sometimes. For a maze with one dot, it doesn't change anything.

# UCS - pacman_ucs.py

This is basically the same as the pacman A*, but the heuristic is replaced by 0. By a fortunate turn of luck, it doesn't run out of recrusion even for the medium maze...

Since there are shortcomings to my A* algorithm (namely, the loop condition), it might be better to use the ucs instead. Indeed, my A* still finds the optimal solution in simple cases (when pacman doesn't need to go back on his feet, mostly), but there is a huge difference in computation time : ucs in about 10 times faster for the medium layout and about twice faster for the small2 layout. If there was a competition to get the maximum amount of points within a given time and with random mazes, ucs would be the way to go.

# UCS, no layer - pacman_ucs_no_layer.py

UCS with no layer is basically the wrong algorithm of astar-no-layer applied to ucs. Yes, there is only one line to change.
